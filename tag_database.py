class TxtDatabase:
	def __init__(self, filename):
		self.f_name = filename
		self.items = []

		self.read_list()

	def read_list(self):
		f = open(self.f_name, 'r')
		for line in f:
			self.items.append(line)
		f.close()

	def exists(self, item):
		for e in self.items:
			if e.replace("\n", "") == item:
				return True
		return False

	def generate_list(self):
		return items;

	def append(self, item):
		f = open(self.f_name, 'a')
		f.write(item)
		f.write("\n")
		f.close()


import sqlite3
import os


class EpisodeDatabase:
	def __init__(self):
		self.db = sqlite3.connect("/media/HDD/Downloads/tvshows.db")

	def createNewTVShow(name, hdd, location, tags):
		from tag_interface import interface
		print ("location" + location)
		if not os.path.exists(location):
			ret = interface.createPrompt("%s does not exist on filesystem, create it?" % (location),
										 interface.YESNO,
										 default='y'
			)
			if ret == "y":
				os.system("mkdir \"%s\"" % (location))
			else:
				print("cannot proceed")
				return

		db = sqlite3.connect("tvshows.db")
		cursor = db.cursor()
		cursor.execute("INSERT INTO tvshow VALUES (?,?,?);", (name, hdd, location))
		insertTags = []
		for tag in tags:
			insertTags.append((name, tag))
		cursor.executemany("INSERT INTO tvtag VALUES (NULL,?,?)", insertTags)
		db.commit()
		cursor.close()

	def getTVShows(self):
		cursor = self.db.cursor()
		cursor.execute("SELECT name FROM tvshow")
		shows_tuples = cursor.fetchall()
		cursor.close()
		shows = []
		for s in shows_tuples:
			shows.append(s[0])
		return shows

	def getTags(self, tvshow):
		cursor = self.db.cursor()
		tags_t = cursor.execute("SELECT tag FROM tvtag WHERE owner=?", (tvshow,))
		tags = []
		for t in tags_t:
			tags.append(t[0])
		return tags

	def getPath(self, tvshow):
		cursor = self.db.cursor()
		cursor.execute("SELECT path FROM tvshow WHERE name=?", (tvshow,))
		path = cursor.fetchone()[0]
		cursor.close()
		return path

	# TODO
	# removeTag(), addTag(), removeShow(), editShow()

		

