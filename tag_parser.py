from tag_database import EpisodeDatabase

class parser:
    options = ("movie","tvshow", "other")

    movie_tags = ["yify", "720p","1080p", "bluray", "x264", "xvid"]

    def __init__ (self, filename):
        self.text = filename		
        self.__parse()



    def __parse (self):
        if (self.isTVShow()):
            self.option = "tvshow"
        elif (self.isMovie()):
            self.option = "movie"
        else:
            self.option = "other"



    def isMovie (self):
        for tag in parser.movie_tags:
            if (self.text.find(tag) != -1):
                return True;
        return False

    def isTVShow (self):
        eD = EpisodeDatabase()

        self.tvshow = ""
        from collections import Counter
        tagOccurrences = Counter()

        for show in eD.getTVShows():
            for tag in eD.getTags(show):
                if self.text.upper().find(tag.upper()) != -1:
                    tagOccurrences[show] += 1

        try:
            self.tvshow = tagOccurrences.most_common(1)[0][0]
            return True
        except:
            return False

