#Seedbox Download Organizer

this is some custom code for moving various files that I download to locations on my NAS.

It can be taylored to your needs by modifying the code but as it stands this code is specific to my setup.

I will be glad to help you with any enquiries, just contact me on my email.

All code is released GPLv3.
