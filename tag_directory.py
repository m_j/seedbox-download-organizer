from tag_database import TxtDatabase
import os


class directory:
	movie_directory = "/media/HDD/Movies"

	def __init__(self, path="/media/HDD/Downloads"):
		self.path = path

	def createDirectoryList(self):
		dir_list = os.listdir(self.path)
		new_dir_list = []
		db = TxtDatabase("tagged.txt")

		for item in dir_list:
			if not db.exists(item):
				new_dir_list.append(item)

		return new_dir_list

	def move(self, filename, moveTo):
		os.system("mv -b \"%s\" \"%s\" &" % (filename, os.path.join(moveTo, filename)))

	def dontShow(self, filename):
		db = TxtDatabase("tagged.txt")
		db.append(filename)
