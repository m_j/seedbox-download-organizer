#! /usr/bin/python

from tag_interface import interface
from tag_directory import directory
from tag_database import EpisodeDatabase


types = ['movie', 'tv show', 'unknown']

d = directory("/media/HDD/Downloads/")
for filename in d.createDirectoryList():
	ret = interface.generatePromptFor(filename)
	print(ret)
	if ret == 'movie':
		d.move(filename, d.movie_directory)
	elif ret == 'other':
		d.dontShow(filename)
	elif ret == 'skip':
		continue
	else:
		#they selected tv show
		eD = EpisodeDatabase()
		path = eD.getPath(ret)
		d.move(filename, path)
