#! /bin/sh
echo "creating required database"

echo "CREATE TABLE tvshow (
name varchar PRIMARY KEY,
hdd varchar,
path varchar UNIQUE NOT NULL
);
CREATE TABLE tvtag (
id INTEGER PRIMARY KEY,
owner REFERENCES tvshow (name),
tag varchar
);" | sqlite3 tvshows.db
