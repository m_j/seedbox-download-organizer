import os
import sys

import tag_parser


class interface:
    YESNO = [['Y', 'es'], ['N', 'o']]

    def __init__(self):
        print("Welcome to htpc dwlparser by Michael Johnston")

    def createPrompt(promptText, options, newline=True, default=''):
        if (newline):
            print(promptText)
        else:
            print(promptText, end='')

        keys = []

        prompt = "[%s]%s" % (options[0][0], options[0][1])
        for option in options:
            keys.append(option[0].lower())
            if option != options[0]:
                prompt += "/"
                prompt += "[%s]%s" % (option[0], option[1])

        if (default != ''):
            prompt += " [%s]? " % (default.upper())
        prompt += " >> "

        while True:
            s = input(prompt).lower()
            if s in keys:
                return s
            if s in [""] and default != '':
                return default.lower()

    def generatePromptFor(filename):
        print(filename)
        parser = tag_parser.parser(filename)
        if parser.option == "movie":
            ret = interface.createPrompt("Is it a movie? ", interface.YESNO, default='y')
            if ret == 'y':
                return "movie"
            else:
                parser.option = "other"

        if parser.option == "tvshow":
            ret = interface.createPrompt("Is it %s" % (parser.tvshow), interface.YESNO, default='y')
            if (ret == 'y'):
                return parser.tvshow
            else:
                parser.option = "other"

        ret = interface.createPrompt("",
                                     [['M', 'ovie'],
                                      ['T', 'vshow'],
                                      ['O', 'ther'],
                                      ['S', 'kip'],
                                      ['X', 'eXit']
                                     ], newline=False, default='s')

        print("");

        if (ret == 'x'):
            sys.exit()
        if (ret == 's'):
            return 'skip'
        if (ret == 'm'):
            return "movie"
        if (ret == 'o'):
            return "other"
        if (ret == 't'):
            return interface.TVShowInterface()


    def TVShowInterface():
        from tag_database import EpisodeDatabase

        eD = EpisodeDatabase()
        tvshows = eD.getTVShows()
        print("[0] Create new TV Show entry")
        for i in range(1, len(tvshows) + 1):
            print("[%d] %s" % (i, tvshows[i - 1]))

        valid = False
        while not valid:
            try:
                ret = int(input(">> "))
                if ret in range(-1, len(tvshows) + 1):
                    valid = True
            except:
                print("not valid number")
                ret = -1

        if (ret == -1):
            sys.exit()

        if ret == 0:
            interface.createNewTVShow()
            return interface.TVShowInterface()

        return tvshows[ret - 1]

    def createNewTVShow():
        showName = input("Enter show name >> ")
        hdd = interface.createPrompt("Select HDD", [['H', 'DD'], ['M', 'ULTIMEDIA']])
        if (hdd == 'h'):
            hdd = 'hdd'
            location = input("Enter path >> /media/HDD/TV/")
            location = os.path.join('/media/HDD/TV', location)
        elif (hdd == 'm'):
            hdd = 'multimedia'
            location = input("Enter path >> /media/MULTIMEDIA/My Videos/TV Shows/")
            location = os.path.join('/media/MULTIMEDIA/My Videos/TV Shows', location)

        tags_str = input("enter space separated list of tags >> ")
        tags = tags_str.split(" ")

        print(
            """Verify TV Show
			name : %s
			location : %s
			tags : %s
			""" % (showName, location, tags)
        )
        ret = interface.createPrompt("Accept", interface.YESNO, default='y')
        if (ret == 'y'):
            from tag_database import EpisodeDatabase
            EpisodeDatabase.createNewTVShow(showName, hdd, location, tags)







